﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace calculator
{

    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        int num1 = -1;
        int num2 = -1;
        char op = 'a';
        public MainPage()
        {
            InitializeComponent();
        }

        void OnClick0(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 0;
                outputLabel.Text = num1.ToString();
            }
            else if(op == 'a')
            {
                num1 = num1 * 10 + 0;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 0;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 0;
                outputLabel.Text = num2.ToString();
            }
           
        }
        void OnClick1(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 1;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 1;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 1;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 1;
                outputLabel.Text = num2.ToString();
            }

        }
        void OnClick2(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 2;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 2;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 2;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 2;
                outputLabel.Text = num2.ToString();
            }

        }
        void OnClick3(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 3;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 3;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 3;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 3;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick4(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 4;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 4;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 4;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 4;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick5(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 5;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 5;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 5;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 5;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick6(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 6;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 6;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 6;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 6;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick7(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 7;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 7;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 7;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 7;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick8(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 8;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 8;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 8;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 8;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClick9(object sender, EventArgs e)
        {
            if (num1 == -1)
            {
                num1 = 9;
                outputLabel.Text = num1.ToString();
            }
            else if (op == 'a')
            {
                num1 = num1 * 10 + 9;
                outputLabel.Text = num1.ToString();
            }
            else if (num2 == -1)
            {
                num2 = 9;
                outputLabel.Text = num2.ToString();
            }
            else
            {
                num2 = num2 * 10 + 9;
                outputLabel.Text = num2.ToString();
            }
        }
        void OnClicka(object sender, EventArgs e)
        {
            op = '+';
        }
        void OnClickb(object sender, EventArgs e)
        {
            op = '-';
        }
        void OnClickc(object sender, EventArgs e)
        {
            op = '/';
        }
        void OnClickd(object sender, EventArgs e)
        {
            op = '*';
        }
        void OnClicke(object sender, EventArgs e)
        {
            int num;
            switch (op)
            {
                case '+':
                    num = num1 + num2;
                    outputLabel.Text = num.ToString();
                    break;
                case '-':
                    num = num1 - num2;
                    outputLabel.Text = num.ToString();
                    break;

                case '/':
                    num = num1 / num2;
                    outputLabel.Text =num.ToString();
                    break;

                case '*':
                    num = num1 * num2;
                    outputLabel.Text = num.ToString();
                    break;
                case 'a':
                    outputLabel.Text = "No Operator";
                    break;
            
            }
            num1 = -1;
            num2 = -1;
            op = 'a';

        }
        void OnClickClear(object sender, EventArgs e)
        {
            num1 = -1;
            num2 = -1;
            op = 'a';
            outputLabel.Text = "0";
        }



    }
}
